﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    #endregion

    #region PARAMS

    public List<Victim> victimsList;

    public List<Vector3> matrix = new List<Vector3>();

    public bool stop = false;

    #endregion

    #region PROPERTIES

    public static VictimManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        CreateMatrix();
    }

    public void CreateMatrix()
    {
        matrix.Clear();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                matrix.Add(new Vector3(i * 3 - 3, 0, j * 3 - 3));
            }
        }
    }

    IEnumerator C_SpawnVictim(float delay)
    {
        yield return new WaitForSeconds(delay);
        Victim victim = VictimPool.Instance.GetFromPool();
        victimsList.Add(victim);
        int rand = (int)Random.Range(0f, matrix.Count);
        Vector3 randPos = matrix[rand];
        if (randPos != Player.Instance.transform.position)
        {
            victim.transform.position = randPos;
        }
        else
        {
            rand++;
            rand = (rand > matrix.Count - 1) ? 0 : rand;
            victim.transform.position = matrix[rand];
        }

        victim.transform.rotation = Quaternion.identity;
        victim.transform.SetParent(PoolLocation.Instance.victimsParent);
    }


    [NaughtyAttributes.Button]
    public void SpawnVictim()
    {
        // StopAllCoroutines();
        if (stop)
            return;
        StartCoroutine(C_SpawnVictim(GameConfiguration.Instance.delayBetweenVictims));
    }

    public void ClearAllVictims()
    {
        foreach (Victim victim in victimsList)
        {
            VictimPool.Instance.ReturnToPool(victim);
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
