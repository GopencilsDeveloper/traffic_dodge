﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera mainCamera;
    // public float standardSize = 10f;
    // public float standardRatio = 9f / 16f;

    private void Start()
    {
        UpdateCamera();
    }

    public void UpdateCamera()
    {
        float screenRatio = Camera.main.aspect;
        if (screenRatio <= 0.475f)  //9:19
        {
            // mainCamera.orthographicSize = standardSize * (standardRatio / (9f / 19f));
            mainCamera.transform.position = new Vector3(0f, 30f, -4f);
        }
        else
        if (screenRatio <= 0.565f)  //9:16
        {
            // mainCamera.orthographicSize = standardSize;
            mainCamera.transform.position = new Vector3(0f, 30f, -4f);
        }
        else
        if (screenRatio <= 0.8)  //3:4
        {
            // mainCamera.orthographicSize = standardSize * (standardRatio / (3f / 4f));
            mainCamera.transform.position = new Vector3(0f, 25f, -4f);
            Debug.Log("Cam");
        }
    }

}
