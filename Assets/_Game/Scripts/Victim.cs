﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Victim : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public Animator animator;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        animator.SetTrigger("Idle");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StackUp();
        }

        if (other.CompareTag("Enemy"))
        {
            Crash(other.gameObject.transform);
        }
    }

    public void StackUp()
    {
        Player.Instance.rescuedVictims.Add(this);
        Player.Instance.AddHeight(1);

        transform.SetParent(Player.Instance.transform);
        animator.SetTrigger("Jump");
        transform.DOLocalJump(new Vector3(0f, Player.Instance.height * 3f, 0f), 1f, 1, 0.2f);
        transform.localRotation = Quaternion.identity;

        Debug.Log("Rescued!");

        VictimManager.Instance.SpawnVictim();
    }

    public void Crash(Transform enemy)
    {
        StartCoroutine(C_Crash(enemy));
    }

    IEnumerator C_Crash(Transform enemy)
    {
        Debug.Log("Crashed!");
        animator.SetTrigger("Falling");
        transform.DOJump(enemy.forward * 15f, 15f, 1, 1f).SetEase(Ease.Flash);
        transform.DORotateQuaternion(Random.rotation, 1f).SetEase(Ease.Flash);

        yield return new WaitForSeconds(1f);
        Debug.Log("Crashed!");

        VictimManager.Instance.victimsList.Remove(this);
        VictimManager.Instance.SpawnVictim();
        VictimPool.Instance.ReturnToPool(this);
    }

    #endregion

    #region DEBUG
    #endregion

}
