﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public List<Sensor> sensorsList;

    // public List<float> hitDistancesList;

    #endregion

    #region PROPERTIES

    public static SensorManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    [NaughtyAttributes.Button]
    public void Sense()
    {
        // Debug.Log("sense");
        foreach (Sensor sensor in sensorsList)
        {
            sensor.Sense();
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
