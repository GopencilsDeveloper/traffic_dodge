﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public Transform enemiesParent;

    #endregion

    #region PARAMS

    public List<Enemy> enemiesList;

    #endregion

    #region PROPERTIES

    public static EnemyManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public int count;
    [NaughtyAttributes.Button]
    public void UpdateEnemiesCount()
    {
        count = 0;
        foreach (Transform child in enemiesParent)
        {
            if (child.gameObject.activeSelf)
            {
                count++;
            }
        }
    }

    public void ClearAllEnemies()
    {
        foreach (Transform child in enemiesParent)
        {
            if (child.gameObject.activeSelf)
            {
                EnemyPool.Instance.ReturnToPool(child.gameObject.GetComponent<Enemy>());
            }
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
