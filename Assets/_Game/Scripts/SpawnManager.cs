﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public List<Spawner> spawnersList;

    public Transform spawnersLeft;
    public Transform spawnersRight;
    public Transform spawnersTop;
    public Transform spawnersDown;

    #endregion

    #region PARAMS

    float stepDuration;

    public List<Enemy> enemiesList;

    public Enemy lastEnemy;


    #endregion

    #region PROPERTIES

    public static SpawnManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public void Initialize(float stepDuration)
    {
        this.stepDuration = stepDuration;
    }

    [Button]
    public void SpawnEndless()
    {
        float rand = Random.value;
        if (rand <= 0.15f)
        {
            SetSpawnerAreas(2);
        }
        else
        if (rand <= 0.3f)
        {
            SetSpawnerAreas(3);
        }
        else
        {
            SetSpawnerAreas(4);
        }

        StartCoroutine(C_SpawnEndless());
    }

    public bool stop = false;
    IEnumerator C_SpawnEndless()
    {
        // StopAllCoroutines();

        while (!stop)
        {
            yield return new WaitForSeconds(stepDuration);
            for (int i = 0; i < GameConfiguration.Instance.spanwerAtSameTime; i++)
            {
                Spawner spawner = spawnersList[(int)Random.Range(0f, spawnersList.Count)];
                if (spawner.gameObject.activeInHierarchy)
                {
                    spawner.SpawnEnemy();
                }
            }
            // SensorManager.Instance.Sense();
            EnemyManager.Instance.UpdateEnemiesCount();
        }
    }

    public void SpawnSpecialEnemy()
    {

    }

    public void SetSpawnerAreas(int count)
    {
        Debug.Log("Count:" + count);
        switch (count)
        {
            case 2:
                spawnersRight.gameObject.SetActive(false);
                spawnersDown.gameObject.SetActive(false);
                spawnersLeft.gameObject.SetActive(true);
                spawnersTop.gameObject.SetActive(true);
                break;

            case 3:
                spawnersRight.gameObject.SetActive(false);
                spawnersDown.gameObject.SetActive(true);
                spawnersLeft.gameObject.SetActive(true);
                spawnersTop.gameObject.SetActive(true);
                break;

            case 4:
                spawnersRight.gameObject.SetActive(true);
                spawnersDown.gameObject.SetActive(true);
                spawnersLeft.gameObject.SetActive(true);
                spawnersTop.gameObject.SetActive(true);
                break;

        }

    }

    #endregion

    #region DEBUG
    #endregion

}
