﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MapManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public Transform mapParent;
    public List<GameObject> mapsList;


    #endregion

    #region PARAMS

    #endregion

    #region PROPERTIES

    public static MapManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    [NaughtyAttributes.Button]
    public void MoveMap()
    {
        float endValue = mapParent.transform.position.z - 60f;
        mapParent.DOMoveZ(endValue, 2f).OnComplete(
            () =>
            {
                GameObject firstMap = mapsList[0];
                firstMap.transform.localPosition = new Vector3(0f, 0f, firstMap.transform.localPosition.z + 300f);
                mapsList.RemoveAt(0);
                mapsList.Insert(mapsList.Count, firstMap);
            }
        );

        Player.Instance.animator.SetTrigger("Jump");

    }

    #endregion

    #region DEBUG
    #endregion

}
