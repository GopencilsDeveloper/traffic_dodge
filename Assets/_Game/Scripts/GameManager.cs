﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public GameState currentState;
    #endregion

    #region PROPERTIES
    public static GameManager Instance { get; private set; }
    #endregion

    #region EVENTS
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
