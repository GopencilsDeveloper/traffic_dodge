﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class GameConfiguration : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    [Header("PLAYER")]
    public Player player;
    public float playerRange;
    public float playerStep;
    public float playerSpeed;

    [Header("ENEMY")]
    public float enemySpeed;
    public float enemySpecialSpeed;

    [Header("SPAWNER")]
    public float spawnStepDuration;
    public int spanwerAtSameTime;
    public float rangeBetweenEnemies;

    [Header("VICTIM")]
    public float delayBetweenVictims;


    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES

    public static GameConfiguration Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    [Button]
    public void Initialize()
    {
        player.Initialize(playerRange, playerStep, playerSpeed);
        SpawnManager.Instance.Initialize(spawnStepDuration);
    }

    private void OnEnable()
    {
        Initialize();
    }

    #endregion

    #region DEBUG
    #endregion

}
