﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Animator animator;
    public Transform playerObj;
    public GameObject END;

    #endregion

    #region PARAMS

    float range;
    float step;
    float moveDuration;
    public bool moving;
    public int height;

    public List<Victim> rescuedVictims = new List<Victim>();

    #endregion

    #region PROPERTIES

    public static Player Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public void Initialize(float range, float step, float moveDuration)
    {
        this.range = range;
        this.step = step;
        this.moveDuration = moveDuration;
    }

    private void OnEnable()
    {
        SwipeManager.OnSwipeDetected += OnSwipeDetected;
        UIManager.Instance.UpdatePercentage(0f);

    }

    private void OnDisable()
    {
        SwipeManager.OnSwipeDetected -= OnSwipeDetected;
    }

    void OnSwipeDetected(Swipe direction, Vector2 swipeVelocity)
    {
        Move(direction);
    }

    public void Refresh()
    {
        playerObj.transform.localPosition = Vector3.zero;
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
        playerObj.transform.localRotation = Quaternion.identity;

        GameConfiguration.Instance.spawnStepDuration = 0.75f;
        GameConfiguration.Instance.enemySpeed = 7.5f;
        GameConfiguration.Instance.Initialize();

        VictimManager.Instance.ClearAllVictims();
        EnemyManager.Instance.ClearAllEnemies();

        SpawnManager.Instance.stop = false;
        VictimManager.Instance.stop = false;
        height = 0;
        UIManager.Instance.UpdatePercentage(0f);
        moving = false;

        VictimManager.Instance.SpawnVictim();
        SpawnManager.Instance.SpawnEndless();
    }

    public void Move(Swipe direction)
    {
        if (moving)
            return;

        switch (direction)
        {

            case Swipe.Up:
                if (transform.position.z >= range)
                    return;

                transform.DOMoveZ(transform.position.z + step, moveDuration)
                .SetEase(Ease.Flash)
                .OnPlay(() => { moving = true; })
                .OnComplete(() => { moving = false; });

                transform.DORotateQuaternion(Quaternion.Euler(0f, 0f, 0f), moveDuration * 0.5f);

                break;

            case Swipe.Down:
                if (transform.position.z <= -range)
                    return;

                transform.DOMoveZ(transform.position.z - step, moveDuration)
                .SetEase(Ease.Flash)
                .OnPlay(() => { moving = true; })
                .OnComplete(() => { moving = false; });

                transform.DORotateQuaternion(Quaternion.Euler(0f, 180f, 0f), moveDuration * 0.5f);


                break;

            case Swipe.Left:
                if (transform.position.x <= -range)
                    return;

                transform.DOMoveX(transform.position.x - step, moveDuration)
                .SetEase(Ease.Flash)
                .OnPlay(() => { moving = true; })
                .OnComplete(() => { moving = false; });

                transform.DORotateQuaternion(Quaternion.Euler(0f, -90f, 0f), moveDuration * 0.5f);


                break;

            case Swipe.Right:
                if (transform.position.x >= range)
                    return;

                transform.DOMoveX(transform.position.x + step, moveDuration)
                .SetEase(Ease.Flash)
                .OnPlay(() => { moving = true; })
                .OnComplete(() => { moving = false; });

                transform.DORotateQuaternion(Quaternion.Euler(0f, 90f, 0f), moveDuration * 0.5f);


                break;

        }
    }

    private void LateUpdate()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Move(Swipe.Up);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Move(Swipe.Down);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Move(Swipe.Left);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Move(Swipe.Right);
        }
#endif
    }


    public void AddHeight(int added)
    {
        height += added;

        CheckPlayer();

        float percent = height / 10f;
        UIManager.Instance.UpdatePercentage(percent);

        if (height >= 7)
        {
            GameConfiguration.Instance.spawnStepDuration = 0.35f;
            GameConfiguration.Instance.enemySpeed = 10f;
            GameConfiguration.Instance.Initialize();
            Debug.Log("HARDER!");
        }

    }

    //Check Pass level
    public void CheckPlayer()
    {
        if (height >= 10)
        {
            LevelUp();
        }
    }

    IEnumerator C_LevelUp()
    {
        
        GameConfiguration.Instance.spawnStepDuration = 0.75f;
        GameConfiguration.Instance.enemySpeed = 7.5f;
        GameConfiguration.Instance.Initialize();

        UIManager.Instance.txtLevelPass.SetActive(true);

        Debug.Log("Level Up");
        height = 0;
        UIManager.Instance.UpdatePercentage(0f);


        SpawnManager.Instance.stop = true;
        VictimManager.Instance.stop = true;
        EnemyManager.Instance.ClearAllEnemies();

        moving = true;
        animator.SetTrigger("Jump");

        if (rescuedVictims.Count > 0)
        {
            foreach (Victim victim in rescuedVictims)
            {
                victim.animator.SetTrigger("Jump");
            }
        }

        yield return new WaitForSeconds(2f);

        foreach (Victim victim in rescuedVictims)
        {
            VictimPool.Instance.ReturnToPool(victim);
        }
        rescuedVictims.Clear();

        transform.DOMove(Vector3.zero, moveDuration)
                .SetEase(Ease.Flash)
                .OnPlay(() => { moving = true; })
                .OnComplete(() => { moving = false; });
        transform.DORotateQuaternion(Quaternion.Euler(0f, 0f, 0f), moveDuration * 0.5f);

        // yield return new WaitForSeconds(1f);
        UIManager.Instance.txtLevelPass.SetActive(false);


        MapManager.Instance.MoveMap();

        EnemyManager.Instance.ClearAllEnemies();

        yield return new WaitForSeconds(2f);

        moving = false;

        SpawnManager.Instance.stop = false;
        VictimManager.Instance.stop = false;
        SpawnManager.Instance.SpawnEndless();
        VictimManager.Instance.SpawnVictim();

        yield return null;
    }

    void LevelUp()
    {
        StartCoroutine(C_LevelUp());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Crash(other.transform);
        }
    }

    public void Crash(Transform enemy)
    {

        moving = true;

        SpawnManager.Instance.stop = true;
        VictimManager.Instance.stop = true;

        Debug.Log("Crashed! --> LOSE!");
        animator.SetTrigger("Falling");
        playerObj.DOJump(enemy.transform.forward * 15f, 15f, 1, 1f).SetEase(Ease.Flash);
        playerObj.DORotateQuaternion(Random.rotation, 1f).SetEase(Ease.Flash);

        if (rescuedVictims.Count > 0)
        {
            foreach (Victim victim in rescuedVictims)
            {
                victim.Crash(enemy.transform);
            }
        }

        END.SetActive(true);

    }


    #endregion

    #region DEBUG
    #endregion

}
