﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum MoveDirection
{
    TOP, DOWN, LEFT, RIGHT
}

[System.Serializable]
public class EnemyVisual
{
    public Mesh mesh;
    public Material material;
}

public class Enemy : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public float rayRange = 10f;
    public BoxCollider boxCollider;

    public List<EnemyVisual> enemyVisualsList;

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    #endregion

    #region PARAMS

    float speed;
    // float range;
    Vector3 rayDirection;
    Quaternion rot;
    public bool moving;
    bool isOutRange = false;
    bool canConflict;


    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Initialize(GameConfiguration.Instance.enemySpeed/* , GameConfiguration.Instance.enemyRange */);
    }

    public void Initialize(float speed/* , float range */)
    {
        boxCollider.enabled = true;
        canConflict = false;
        this.isOutRange = false;
        this.speed = speed;
        // this.range = range;
    }

    public void Move(MoveDirection direction)
    {

        switch (direction)
        {

            case MoveDirection.TOP:
                this.rot = Quaternion.Euler(0f, 0f, 0f);
                break;

            case MoveDirection.DOWN:
                this.rot = Quaternion.Euler(0f, 180f, 0f);

                break;

            case MoveDirection.LEFT:
                this.rot = Quaternion.Euler(0f, -90f, 0f);

                break;

            case MoveDirection.RIGHT:
                this.rot = Quaternion.Euler(0f, 90f, 0f);

                break;

        }
        transform.rotation = rot;
        moving = true;
        CheckFront();
    }


    private void Update()
    {
        if (moving)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime);

            if ((Mathf.Abs(transform.position.x) >= 22f || Mathf.Abs(transform.position.z) >= 22f) && !isOutRange)
            {
                EnemyPool.Instance.ReturnToPool(this);

                isOutRange = true;
            }
        }
    }

    //Ignore Enemies same lane.
    public void CheckFront()
    {
        if (Physics.Raycast(transform.position, transform.forward, rayRange * 2f, 1 << 8))
        {
            EnemyPool.Instance.ReturnToPool(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Zone"))
        {
            canConflict = true;
        }

        if (other.CompareTag("Enemy") && canConflict)
        {
            OnConflict();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Zone"))
        {
            canConflict = false;
        }
    }

    public void Explode()
    {
        
    }

    public void OnConflict()
    {
        StartCoroutine(C_OnConflict());
    }

    IEnumerator C_OnConflict()
    {
        boxCollider.enabled = false;

        moving = false;
        canConflict = false;
        transform.DOJump(-transform.forward * 15f, 15f, 1, 1f).SetEase(Ease.Flash);
        transform.DORotateQuaternion(Random.rotation, 1f).SetEase(Ease.Flash);
        yield return new WaitForSeconds(1f);
        EnemyPool.Instance.ReturnToPool(this);

    }

    public void SetVisual(int index)
    {
        EnemyVisual enemyVisual = enemyVisualsList[index];
        meshFilter.mesh = enemyVisual.mesh;
        meshRenderer.material = enemyVisual.material;
    }

    #endregion

    #region DEBUG
    #endregion

}
