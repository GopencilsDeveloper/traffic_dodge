﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;


    public Image imgFill;
    public GameObject txtLevelPass;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static UIManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }

    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }

    public void OnWinGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);
    }

    public void OnLoseGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
    }

    public void UpdatePercentage(float percent)
    {
        imgFill.fillAmount = percent;
    }

    #endregion
}