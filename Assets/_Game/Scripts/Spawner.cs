﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public MoveDirection moveDirection;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    [Button]
    public void SpawnEnemy()
    {
        Enemy enemy = EnemyPool.Instance.GetFromPool();
        enemy.transform.SetParent(PoolLocation.Instance.enemiesParent);
        enemy.transform.position = this.transform.position;
        enemy.transform.rotation = Quaternion.identity;
        enemy.Move(moveDirection);
        SpawnManager.Instance.lastEnemy = enemy;
    }

    #endregion

    #region DEBUG
    #endregion

}
