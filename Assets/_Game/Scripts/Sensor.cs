﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS

    public List<RaycastHit> hitsList = new List<RaycastHit>();

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public void Sense()
    {
        hitsList.Clear();

        //Ray Top
        RaycastHit hitT;
        Ray rayT = new Ray(transform.position, transform.forward);
        // Debug.DrawRay(transform.position, transform.forward * 25f, Color.red, 100f);
        if (Physics.Raycast(rayT, out hitT, 25f, 1 << 8))
        {
            // Debug.Log("Hit Enemy Top : " + hitT.distance);
            hitsList.Add(hitT);
        }

        //Ray Down
        RaycastHit hitD;
        Ray rayD = new Ray(transform.position, -transform.forward);
        // Debug.DrawRay(transform.position, -transform.forward * 25f, Color.red, 100f);
        if (Physics.Raycast(rayD, out hitD, 25f, 1 << 8))
        {
            // Debug.Log("Hit Enemy Down : " + hitD.distance);
            hitsList.Add(hitD);

        }

        //Ray Left
        RaycastHit hitL;
        Ray rayL = new Ray(transform.position, -transform.right);
        // Debug.DrawRay(transform.position, -transform.right * 25f, Color.red, 100f);
        if (Physics.Raycast(rayL, out hitL, 25f, 1 << 8))
        {
            // Debug.Log("Hit Enemy Left : " + hitL.distance);
            hitsList.Add(hitL);

        }

        //Ray Right
        RaycastHit hitR;
        Ray rayR = new Ray(transform.position, transform.right);
        // Debug.DrawRay(transform.position, transform.right * 25f, Color.red, 100f);
        if (Physics.Raycast(rayR, out hitR, 25f, 1 << 8))
        {
            // Debug.Log("Hit Enemy Right : " + hitR.distance);
            hitsList.Add(hitR);

        }
        CheckConflict();
    }

    public void CheckConflict()
    {
        if (hitsList.Count < 2)
        {
            return;
        }
        else
        {
            bool isConflict1 = false;
            bool isConflict2 = false;
            if (hitsList.Count == 2)
            {
                isConflict1 = (Mathf.Abs(hitsList[0].distance - hitsList[1].distance) <= 2.5f) ? true : false;
                // Debug.Log("isConflict: " + isConflict1);
                if (isConflict1)
                {
                    GameObject hitObject = hitsList[0].collider.gameObject;
                    GameObject hitObject1 = hitsList[1].collider.gameObject;
                    if ((Mathf.Abs(hitObject.transform.position.x) > 15f) || (Mathf.Abs(hitObject.transform.position.z) > 15f))
                    {
                        EnemyPool.Instance.ReturnToPool(hitObject.GetComponent<Enemy>());
                        EnemyPool.Instance.ReturnToPool(hitObject1.GetComponent<Enemy>());
                        Debug.Log("Conflict Resovled");
                    }
                }
            }
            else
            if (hitsList.Count == 3)
            {
                bool t = (Mathf.Abs(hitsList[0].distance - hitsList[1].distance) <= 2.5f) ? true : false;
                bool t1 = (Mathf.Abs(hitsList[0].distance - hitsList[2].distance) <= 2.5f) ? true : false;
                bool t2 = (Mathf.Abs(hitsList[1].distance - hitsList[2].distance) <= 2.5f) ? true : false;

                isConflict2 = t || t1 || t2;
                // Debug.Log("isConflict: " + isConflict2);

                if (t)
                {
                    GameObject hitObject = hitsList[0].collider.gameObject;
                    GameObject hitObject1 = hitsList[1].collider.gameObject;
                    if ((Mathf.Abs(hitObject.transform.position.x) > 15f) || (Mathf.Abs(hitObject.transform.position.z) > 15f))
                    {
                        EnemyPool.Instance.ReturnToPool(hitObject.GetComponent<Enemy>());
                        EnemyPool.Instance.ReturnToPool(hitObject1.GetComponent<Enemy>());
                        Debug.Log("Conflict Resovled");
                    }
                }
                if (t1)
                {
                    GameObject hitObject = hitsList[2].collider.gameObject;
                    GameObject hitObject1 = hitsList[0].collider.gameObject;
                    if ((Mathf.Abs(hitObject.transform.position.x) > 15f) || (Mathf.Abs(hitObject.transform.position.z) > 15f))
                    {
                        EnemyPool.Instance.ReturnToPool(hitObject.GetComponent<Enemy>());
                        EnemyPool.Instance.ReturnToPool(hitObject1.GetComponent<Enemy>());

                        Debug.Log("Conflict Resovled");
                    }

                }
                if (t2)
                {
                    GameObject hitObject = hitsList[1].collider.gameObject;
                    GameObject hitObject1 = hitsList[2].collider.gameObject;
                    if ((Mathf.Abs(hitObject.transform.position.x) > 15f) || (Mathf.Abs(hitObject.transform.position.z) > 15f))
                    {
                        EnemyPool.Instance.ReturnToPool(hitObject.GetComponent<Enemy>());
                        EnemyPool.Instance.ReturnToPool(hitObject1.GetComponent<Enemy>());
                        Debug.Log("Conflict Resovled");
                    }
                }
            }
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
