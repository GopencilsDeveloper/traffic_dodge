﻿using UnityEngine;

public class PoolLocation : MonoBehaviour
{
    public static PoolLocation Instance { get; private set; }

    public Transform victimsParent;
    public Transform enemiesParent;

    private void Awake()
    {
        Instance = this;
    }
}
